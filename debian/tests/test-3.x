#!/bin/bash

exec 2>&1
set -ex

echo "test redir" > input

nc -q 0 -l -p 9000 < input &
sleep 1

redir 127.0.0.1:5000 127.0.0.1:9000
sleep 1

nc 127.0.0.1 5000 > output

diff -u input output
