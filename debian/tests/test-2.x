#!/bin/bash

exec 2>&1
set -ex

echo "test redir" > input

nc -q 0 -l -p 9000 < input &
pid_nc=$!
sleep 1

redir --laddr=127.0.0.1 --lport=5000  --caddr=127.0.0.1 --cport=9000 &
pid_redir=$!
sleep 1

nc 127.0.0.1 5000 > output

function clean_up {
  kill -9 $pid_redir
}
trap clean_up EXIT SIGHUP SIGINT SIGTERM

diff -u input output
